package main

import (
	"fmt"
	"log"
	"piefbie/pkg/api"
	"piefbie/pkg/config"

	"github.com/gin-gonic/gin"
)

func main() {
	config, err := config.LoadConfig("./config/")
	if err != nil {
		log.Fatal("cant read", err)
	}

	// debug
	fmt.Println(config.Server.Port)

	r := gin.Default()
	gin.SetMode(config.Server.Mode)
	api.RegisterRouter(r)
	r.Run(config.Server.Port)

	// fmt.Println("init hello")
}
