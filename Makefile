# cluster variable
CLUSTER_NAME="piefbie-cluster"
CLUSTER_VERSION="kindest/node:v1.22.9"

# apps variable
GitCommit := $(shell git rev-parse --short HEAD)
LDFLAGS := "-w -s -X piefbie/pkg/version.GitCommit=$(GitCommit)"

.DEFAULT_GOAL := help
.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

hello: ## init hello
		@echo "hello!"

build: ## build binary
		@CGO_ENABLED=0 go build -o dist/piefbie -ldflags $(LDFLAGS) cmd/piefbie/main.go

build-container: ## build container
		@docker build -t piefbie .

# run from terminal:
# docker run --rm -p 9999:9999 -v $(pwd)/config/config.yaml:/cmd/piefbie/config/config.yaml piefbie:latest
run-container: ## run container
		@docker run --rm -p 9999:9999 -v $(CURDIR)/config/config.yaml:/cmd/piefbie/config/config.yaml piefbie:latest

kube-up: ## start kind cluster
		@kind create cluster --name $(CLUSTER_NAME) --config hack/kind-cluster.yaml --image $(CLUSTER_VERSION)
		@kubectl cluster-info --context kind-$(CLUSTER_NAME)
		@kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml

kube-cert:
		@kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.9.1/cert-manager.yaml
		@kubectl apply -f hack/https-mode/all-in-one.yaml

kube-argocd: ## run argocd
		@kubectl create namespace argocd
		@kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.9.1/cert-manager.yaml
		@kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
		@kubectl apply -f hack/argocd-ingress.yaml

kube-down: ## delete kind cluster
		@kind delete cluster --name $(CLUSTER_NAME)