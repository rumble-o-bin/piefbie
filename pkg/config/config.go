package config

import "github.com/spf13/viper"

type Configuration struct {
	Server ServerConfigurations
}

type ServerConfigurations struct {
	Port string
	Mode string
}

func LoadConfig(path string) (config Configuration, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}
