package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func RegisterRouter(r *gin.Engine) {
	// 404 handler
	r.NoRoute(func(c *gin.Context) {
		c.String(http.StatusNotFound, "Incorrect request route.")
	})

	r.GET("/", InfoHandler)
	r.GET("/ping", Ping)
	r.GET("/print", PrintRequest)
}
