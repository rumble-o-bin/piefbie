package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func PrintRequest(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"request_header": c.Request.Header,
	})
}
