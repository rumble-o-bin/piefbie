package api

import (
	"net/http"
	"os"
	"piefbie/pkg/version"
	"runtime"
	"strconv"

	"github.com/gin-gonic/gin"
)

type InfoResponse struct {
	GOOS         string `json:"goos"`
	GOARCH       string `json:"goarch"`
	Runtime      string `json:"runtime"`
	NumGoroutine string `json:"num_goroutine"`
	Commit       string `json:"commit"`
	Cookie       string `json:"cookie"`
	PodName      string `json:"podname"`
	Namespace    string `json:"namespace"`
}

func InfoHandler(c *gin.Context) {

	data := InfoResponse{
		GOOS:         runtime.GOOS,
		GOARCH:       runtime.GOARCH,
		Runtime:      runtime.Version(),
		NumGoroutine: strconv.FormatInt(int64(runtime.NumGoroutine()), 10),
		Commit:       version.GitCommit,
		PodName:      os.Getenv("POD_NAME"),
		Namespace:    os.Getenv("POD_NAMESPACE"),
	}

	c.JSON(http.StatusOK, data)
}
