# first builder
FROM golang:1.18.1-alpine as builder

ENV APP_DIR /go/src/piefbie
RUN apk --no-cache add build-base git mercurial gcc
WORKDIR ${APP_DIR}
COPY . ${APP_DIR}
RUN cd ${APP_DIR} \
    go mod tidy \
    make build

# second runtime
FROM alpine:3.15

ENV APP_BINARY /go/src/piefbie/dist
WORKDIR /cmd/piefbie
COPY --from=builder ${APP_BINARY}/piefbie .
CMD ["./piefbie"]
